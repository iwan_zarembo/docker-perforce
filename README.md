Docker Perforce Servers
=======================

[![License: MIT](http://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](LICENSE)

The repository contains a list of docker images to run a few perforce servers.

* [iwzr/perforce-base](base) - Base container, which includes the Perforce APT repositories and the required tools.
*  [iwzr/perforce-helixcore](helixCore) - Perforce Helix Server (P4D). See also [Perforce HelixCore](https://www.perforce.com/products/helix-core) for more information.
*  [iwzr/perforce-helix4git](helix4git) - Base container, includes the Perforce APT repositories. See also [Perforce Helix4Git](https://www.perforce.com/products/helix4git) for more information.

## Contributing
Contributing is always welcome and appreciated! Pull requests and issues will
be processed as fast as possible!

Issues can be opened at [Bitbucket Issues](https://bitbucket.org/iwan_zarembo/docker-perforce/issues/new).
