#!/bin/bash
set -e

# Perforce paths
CONFIGURE_SCRIPT=/opt/perforce/git-connector/bin/configure-git-connector.sh
CONFIG_ROOT=/configs
CONFIG_FILENAME=gconn.conf
_CONN_HOME=/opt/perforce/git-connector

###################################################################################################
## SSH Related
###################################################################################################

# checks if the key with the alrithm is existing and if not, then it will generate it
# and add it to the sshd_config.
# Usage; addSshKey algorithm
# Example: addSshKey rsa
function addSshKey() {
  local _alg=$1
  local _key=/etc/ssh/ssh_host_${_alg}_key
  echo "checking for key $_key"
  if [ ! -e "$_key" ] ; then
    ssh-keygen -f $_key -N '' -t $_alg
    echo "adding generated key $_key"
    echo "HostKey $_key" >> /etc/ssh/sshd_config
  fi
}

if [ -d "$CONFIG_ROOT/ssh" ] ; then
  echo "Configuring SSH, since the $CONFIG_ROOT/ssh folder is existing"
  if [ -e "$CONFIG_ROOT/ssh/sshd_config" ] ; then
    echo "Custom sshd_config provided. Replacing the default one."
    if [ -e "/etc/ssh/sshd_config" ] ; then
      echo "deleting the old sshd_config file from /etc/ssh/"
      rm -f /etc/ssh/sshd_config
    fi
    echo "linking the provided from $CONFIG_ROOT/ssh/sshd_config to /etc/ssh/sshd_config"
    ln -s $CONFIG_ROOT/ssh/sshd_config /etc/ssh/sshd_config
  else
    echo "No custom sshd_config found, just adding the keys if available."
    for i in $CONFIG_ROOT/ssh/ssh_host_*_key; do
      echo "adding $i"
      echo "HostKey $i" >> /etc/ssh/sshd_config
    done
  fi
else

  echo "SSH configuration not provided. Generating new host keys for this container if not existing"
  addSshKey rsa
  addSshKey dsa
  addSshKey ecdsa
  addSshKey ed25519
fi

# add privilege seperation dir manually
# see https://askubuntu.com/questions/1109934/ssh-server-stops-working-after-reboot-caused-by-missing-var-run-sshd/1110843#1110843
if [ ! -d "/var/run/sshd" ] ; then
  echo "privilege seperation directory at /var/run/sshd missing, creating it manually"
  mkdir -p /var/run/sshd
fi
echo "Starting SSH server"
echo "root:${ROOT_PWD}" | chpasswd
/usr/sbin/sshd -D &

###################################################################################################
## Apache Related before git-connector configuration
###################################################################################################
# stop apache and reconfigure
service apache2 stop

# configure ssl certificate
if [ ! -d "$CONFIG_ROOT/apache2" ] ; then
  echo "Config folder does not have an apache2 folder, creating it now."
  mkdir -p $CONFIG_ROOT/apache2
fi
if [ -d "$CONFIG_ROOT/apache2/ssl" ] ; then
  echo "The folder $CONFIG_ROOT/apache2/ssl exists"
  if [ -d "/etc/apache2/ssl" ] ; then
    echo "moving /etc/apache2/ssl to /etc/apache2/ssl.default"
    mv /etc/apache2/ssl /etc/apache2/ssl.default
  fi
  echo "create symlink from $CONFIG_ROOT/apache2/ssl to /etc/apache2/ssl"
  ln -s $CONFIG_ROOT/apache2/ssl /etc/apache2/ssl
elif [ -d "/etc/apache2/ssl" ] ; then
  echo "moving /etc/apache2/ssl to $CONFIG_ROOT/apache2/ssl"
  mv /etc/apache2/ssl $CONFIG_ROOT/apache2/ssl
  echo "create symlink from $CONFIG_ROOT/apache2/ssl to /etc/apache2/ssl"
  ln -s $CONFIG_ROOT/apache2/ssl /etc/apache2/ssl
fi

if [ -d "$CONFIG_ROOT/apache2/sites-available" ] ; then
  echo "The folder $CONFIG_ROOT/apache2/sites-available exists"
  echo "Generating link from $CONFIG_ROOT/apache2/sites-available to /etc/apache2/sites-available"
  mv /etc/apache2/sites-available /etc/apache2/sites-available.bak
  ln -s $CONFIG_ROOT/apache2/sites-available /etc/apache2/sites-available
  enableGconfSite
elif [ -d "/etc/apache2/sites-available" ] ; then
  echo "moving apache site config from /etc/apache2/sites-available to $CONFIG_ROOT/apache2/sites-available"
  mv /etc/apache2/sites-available $CONFIG_ROOT/apache2/sites-available
  ln -s $CONFIG_ROOT/apache2/sites-available /etc/apache2/sites-available
fi

# Checks if the apache config is already enabled and if not then enable it.
if [ -d "/etc/apache2/sites-enabled/$CONFIG_FILENAME" ] ; then
  echo "git connector site not enabled, enabling if now"
  a2ensite $CONFIG_FILENAME
fi

###################################################################################################
## Git Connector Related
###################################################################################################
# add the scripts into the config folder if they are not available
if [ ! -e $CONFIG_ROOT/bin ]; then
  echo "linking bin directory from /opt/perforce/bin to $CONFIG_ROOT/bin"
  ln -s /opt/perforce/bin $CONFIG_ROOT/bin
fi
# Check if the connector was configured. If not, configure it.
if [ ! -e $CONFIG_ROOT/$CONFIG_FILENAME ]; then
    echo "Git Connector not configured, configuring..."

    # If the root path already exists, we're configuring an existing server
    $CONFIGURE_SCRIPT -n ${@}
fi

###################################################################################################
## Apache Related after git-connector configuration
###################################################################################################
# no check if it is already started, will not harm anything
service apache2 start

# block the process to keep the container running
/bin/sh
