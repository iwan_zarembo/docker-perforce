Perforce base image
===================

[![License: MIT](http://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](../LICENSE)

The base image for all images. The docker image is based on *ubuntu:bionic* and
contains the [Perforce APT repositories](http://package.perforce.com/).

All images which are based on this image should use the environment variable **BASE_UBUNTU_VERSION**.

The consumers of this base image are:

*  [iwzr/perforce-helixcore](perforce-helixcore) - Perforce Helix Server (P4D). See also [Perforce HelixCore](https://www.perforce.com/products/helix-core) for more information.
*  [iwzr/perforce-helix4git](perforce-helix4git) - Perforce Helix4Git Serve. See also [Perforce Helix4Git](https://www.perforce.com/products/helix4git) for more information.
