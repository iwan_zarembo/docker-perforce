#!/bin/bash
set -e

# Prints the usage of the script.
# The function does not have any parameters.
function printUsage() {
    echo "usage: run.sh [-s server_name] [-p perforce:1666] [-u username] [-a user_password]"
    echo "Used parameters are:"
    echo "  -s servername <name> The internal server name to use. Default is the docker hostname."
    echo "  -p p4port <p4port>   The perforce port to use. Default is ssl:1666"
    echo "  -u p4user <p4user>   The perforce admin user name to use. Default is p4admin"
    echo "  -a p4passwd <pwd>    The mandatory password of the perforce admin user. Default is p4admin123"
    echo "  -h help              Print this help."
    exit 1
}


# Perforce paths
CONFIGURE_SCRIPT=/opt/perforce/sbin/configure-perforce-server.sh
SERVERS_ROOT=/servers
CONFIG_ROOT=/etc/perforce/p4dctl.conf.d

_SERVER_NAME=${SERVER_NAME:-p4host}
_P4PORT=${P4PORT:-ssl:1666}
_P4USER=${P4USER:-p4admin}
_P4PASSWD=${P4PASSWD:-p4admin123}

while (( "$#" )); do
  case "$1" in
    -p|--p4port)
      _P4PORT=$2
      echo "The P4PORT has been provided as a parameter and set to $_P4PORT"
      shift 2
      ;;
    -u|--p4user)
      _P4USER=$2
      echo "The Perforce user name has been provided as a parameter and set to $_P4USER"
      shift 2
      ;;
    -a|--p4passwd)
      _P4PASSWD=$2
      echo "The password of the perforce user has been provided"
      shift 2
      ;;
    -s|--servername)
      _SERVER_NAME=$2
      echo "The server name has been provided as a parameter and set to $_SERVER_NAME."
      shift 2
      ;;
    -h|--help)
      printUsage
      ;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
    *) # preserve positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

SERVER_ROOT=$SERVERS_ROOT/$_SERVER_NAME
# Check if the server was configured. If not, configure it.
if [ ! -f $CONFIG_ROOT/$_SERVER_NAME.conf ]; then
    echo "Perforce server $_SERVER_NAME not configured, configuring."

    # If the root path already exists, we're configuring an existing server
    echo "Running command: $CONFIGURE_SCRIPT -n -r $SERVER_ROOT -p $_P4PORT -u $_P4USER -P *** $_SERVER_NAME"

    $CONFIGURE_SCRIPT -n \
        -r $SERVER_ROOT \
        -p $_P4PORT \
        -u $_P4USER \
        -P $_P4PASSWD \
        $_SERVER_NAME

    echo Server info:
    p4 -p $_P4PORT info
else
    echo "Perforce server $_SERVER_NAME is already configured, starting..."
    # Configuring the server also starts it, if we've not just configured a
    # server, we need to start it ourselves.
    p4dctl start $_SERVER_NAME
fi

# Pipe server echo and wait until the server dies
PID_FILE=/var/run/p4d.$_SERVER_NAME.pid
exec /usr/bin/tail --pid=$(cat $PID_FILE) -n 0 -f "$SERVER_ROOT/logs/log"
