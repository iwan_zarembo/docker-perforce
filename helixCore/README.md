Perforce Helix Core
===================
[![License: MIT](http://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](../LICENSE)

This is a docker image for the [Perforce Helix Server](http://www.perforce.com/).

Usage
The image supports environment variables and parameters. It is up to you, what you want to use.

Using Environment variables:
````
    docker run  -e SERVER_NAME=server-name -e P4PASSWD=<password> \
                -e P4USER=adminuser -e P4PORT=1666 \
                -p 1666:1666 -v {PWD}:/servers \
                iwzr/perforce-helixcore:latest
````

Using parameters:
````
    docker run -p 1666:1666 -v {PWD}:/servers iwzr/perforce-helixcore:latest \
        --servername server-name \
        --p4port 1666 \
        --p4user p4admin \
        --p4passwd some_super_password
````
Do not forget to map the ports. If you choose port 1666 as P4PORT, then you also have to map it
using **-p**.

You can also use the help from the scrip (-h|--help) to get this information.
````
    docker run -p 1666:1666 -v {PWD}:/servers iwzr/perforce-helixcore:latest
````

You can also use the default settings to run the image. Just map the port and the volume:


Details
-------
The following environment variables are available:

  - SERVER_NAME - Server name. Default to **p4host**.
  - P4PORT - Address on which to listen. Described [here](http://www.perforce.com/perforce/doc.current/manuals/cmdref/P4PORT.html). Defaults to **ssl:1666**.
  - P4USER - Superuser username. Only created when creating a new server. Defaults to **p4admin**.
  - P4PASSWD - Superuser password. Required when creating a new server. Defaults to **p4admin123**

The following entry point parameters are supported:

* -s | --servername - The internal server name to use. Defaults to **p4host**."
* -p | --p4port - The perforce port to use. Default is **ssl:1666**"
* -u | --p4user - The perforce admin user name to use. Default is **p4admin**"
* -a | --p4passwd - The mandatory password of the perforce admin user. Default is **p4admin123**"
* -h | --help - Print the help of the script.

Volumes
-------
The image has only one volume `/servers`. It is used to store the server data. So you
usually want to mount it, if you want to keep your data.
